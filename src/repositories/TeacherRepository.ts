import { Like, EntityRepository, Repository } from 'typeorm';
import Teacher from '../models/Teacher';

@EntityRepository(Teacher)
export default class TeacherRepository extends Repository<Teacher> {
  public async findByName(name: string): Promise<Teacher[]> {
    const res = name;
    return this.find({
      where: {
        name: Like(`%${res}%`),
      },
    });
  }
}
