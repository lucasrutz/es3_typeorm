import { Router } from 'express';
import { getRepository, getCustomRepository } from 'typeorm';
import Lesson from '../models/Lesson';
import LessonRepository from '../repositories/LessonRepository';

const lessonRouter = Router();

lessonRouter.post('/', async (request, response) => {
  try {
    const repo = getCustomRepository(LessonRepository);
    return await repo.addLesson(request, response);
  } catch (err) {
    return response.status(200).json({ error: err.message });
  }
});

lessonRouter.get('/', async (request, response) => {
  response.json(await getRepository(Lesson).find());
});

lessonRouter.put('/:id', async (request, response) => {
  try {
    const repo = getCustomRepository(LessonRepository);
    return await repo.updateLesson(request, response);
  } catch (err) {
    return response.status(200).json({ error: err.message });
  }
});
lessonRouter.put('/:id', async (request, response) => {
  const { id } = request.params;
  const repo = getRepository(Lesson);
  const toUpdate = await repo.findOne(id);
  if (!toUpdate) {
    return response.status(201).json({ message: 'Lesson não encontrada.' });
  }

  const { description, classe } = request.body;

  if (description) {
    toUpdate.description = description;
  }

  if (classe) {
    toUpdate.classe = classe;
  }

  const res = await repo.save(toUpdate);

  return response.status(201).json(res);
});
export default lessonRouter;
