import { Router } from 'express';
import { getRepository } from 'typeorm';
import Class from '../models/Class';
import ClassRepository from '../repositories/ClassRepository';

const classRouter = Router();

classRouter.post('/', async (request, response) => {
  try {
    const repo = getRepository(Class);
    const res = await repo.save(request.body);
    return response.status(201).json(res);
  } catch (err) {
    console.log('err.message :>> ', err.message);
    return response.status(400).send();
  }
});

classRouter.get('/', async (request, response) => {
  response.json(await getRepository(Class).find());
});

classRouter.delete('/:id', async (request, response) => {
  try {
    const repo = getRepository(Class);
    const res = await repo.delete(request.params.id);
    return response.status(201).json(res);
  } catch (err) {
    return response.status(400).json({ error: err.message });
  }
});

classRouter.put('/:id', async (request, response) => {
  const { id } = request.params;
  const repo = getRepository(Class);
  const toUpdate = await repo.findOne(id);
  if (!toUpdate) {
    return response.status(201).json({ message: 'Class não encontrada.' });
  }

  const { name, duration, lesson } = request.body;

  if (name) {
    toUpdate.name = name;
  }

  if (duration) {
    toUpdate.duration = parseInt(duration, 10);
  }

  if (lesson) {
    toUpdate.lessons = lesson;
  }

  const res = await repo.save(toUpdate);

  return response.status(201).json(res);
});
export default classRouter;
