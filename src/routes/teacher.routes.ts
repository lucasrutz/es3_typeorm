import { Router } from 'express';
import { getRepository, getCustomRepository } from 'typeorm';
import { validate } from 'class-validator';
import Teacher from '../models/Teacher';
import TeacherRepository from '../repositories/TeacherRepository';

const teacherRouter = Router();

teacherRouter.post('/', async (request, response) => {
  try {
    const repo = getRepository(Teacher);
    const { key, name, email, graduation } = request.body;

    const teacher = repo.create({
      key,
      name,
      email,
      graduation,
    });

    const errors = await validate(teacher);

    if (errors.length === 0) {
      const res = await repo.save(teacher);
      return response.status(201).json(res);
    }
    return response.status(400).json(errors);
  } catch (err) {
    console.log('err.message :>> ', err.message);
    return response.status(400).send();
  }
});

teacherRouter.post('/', async (request, response) => {
  try {
    const repo = getRepository(Teacher);
    const res = await repo.save(request.body);
    return response.status(201).json(res);
  } catch (err) {
    console.log('err.message :>> ', err.message);
    return response.status(400).send();
  }
});

teacherRouter.get('/', async (request, response) => {
  response.json(await getRepository(Teacher).find());
});
teacherRouter.get('/:name', async (request, response) => {
  const repository = getCustomRepository(TeacherRepository);
  const res = await repository.findByName(request.params.name);
  response.json(res);
});
export default teacherRouter;
