import { Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { MaxLength, MinLength, IsEmail } from 'class-validator';

export default abstract class Pessoa {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  @MaxLength(50, { message: 'Nome precisar no máximo 50 caracteres' })
  @MinLength(2, { message: 'Nome deve possuir no mínimo 1 caractere' })
  name: string;

  @CreateDateColumn({ name: 'created_At' })
  createdAt: Date;

  @CreateDateColumn({ name: 'updated_At' })
  updatedAt: Date;
}
