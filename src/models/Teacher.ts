import { Entity, Column, ManyToMany, JoinTable, OneToMany } from 'typeorm';

import { Max, Min } from 'class-validator';
import Person from './Person';
import Student from './Student';
import Lesson from './Lesson';

@Entity('teacher')
export default class Teacher extends Person {
  @Column()
  @Max(99999, { message: 'Chave inválida' })
  @Min(10000)
  key: number;

  @Column()
  graduation: string;

  @ManyToMany(type => Student)
  @JoinTable()
  students: Student;

  @OneToMany(type => Lesson, teacher => Teacher)
  lessons: Lesson[];
}
