import { Entity, Column, ManyToMany, JoinTable } from 'typeorm';

import { Max, Min } from 'class-validator';
import Class from './Class';
import Person from './Person';

@Entity('student')
export default class Student extends Person {
  @Column()
  @Max(99999, { message: 'Chave inválida' })
  @Min(10000)
  key: number;

  @ManyToMany(type => Class)
  @JoinTable()
  classes: Class;
}
